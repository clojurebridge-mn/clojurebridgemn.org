![Happy ClojureBridge MN participants](/assets/images/cbmn-group.jpg "Happy ClojureBridge MN participants")

## Free, beginner-friendly <br> Clojure workshops <br> for women, trans, genderqueer,<br>and gender non-conforming people

<section class="flex future-events">
    <div class="event-image">
        <img src="/assets/images/cbmn-photo-1.jpg" alt="TA and participant sharing Clojure knowledge">
    </div>
    <div class="event-info">
        <h3>Upcoming Events</h3>
        <p>
            <span>Register for the October 5-6, 2018 event on <a href="https://www.eventbrite.com/e/clojurebridge-intro-to-programming-workshop-october-2018-tickets-44285106947" target="_blank">Eventbrite</a>
            </span>
        </p>
    </div>
</section>

> ![ClojureBridge Logo](/assets/images/clojurebridge-logo.png "ClojureBridge Logo")

> ![Clojure Logo](/assets/images/clojure-logo.png "Clojure Logo")

<section class="flex get-involved">
    <div class="involved-image">
        <img src="/assets/images/cbmn-photo-2.jpg">
    </div>
    <div class="involved-info">
        <h3>Get Invovled</h3>
        <ul>
            <li>
                Attend a workshop as a participant or TA
            </li>
            <li>
                Make a donation
            </li>
            <li>
                Connect with the local Clojure community at
                <a href="https://www.meetup.com/clojuremn/">
                    clojure.mn
                </a>
            </li>
            <li>
                Follow us on
                <a href="https://twitter.com/clojurebridgemn?lang=en">
                    Twitter
                </a>
            </li>
        </ul>
    </div>
</section>
